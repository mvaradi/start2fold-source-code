# README #

This is the README of the source code of the release version of Start2Fold. The steps that are necessary to get the application up and running are detailed below.

Start2Fold is a comprehensive collection of carefully curated and classified residue- or segment-level data on the folding and/or stability proteins that have been previously investigated by solvent exchange-based measurements. Besides the classification of residues based on their exchange protection levels, all entries contain the detailed descriptions of the underlying experimental procedures, sample components, measurement conditions, cross references to other databases, references to the original publications and instant visualisation of the relevant residue groups on the 3D structures of the corresponding proteins.
### What is this repository for? ###

* This is the source code of the Start2Fold database
* v.1.0.0 (Release version)

### How do I get set up? ###

#### Summary of set up

Briefly, the steps to set up Start2Fold locally are as follows:

* Clone the repo into your Django project folder (create one if necessary)
* Add the start2fold app to your Django project by modifying the settings.py and the urls.py files
* Run python manage.py runserver 
* The dev version will run at host:port/start2fold

#### Dependencies

Start2Fold depends on Django (1.8.6) and SQL

### Contribution guidelines ###

Feel free to contribute by reviewing or improving the code in any way you feel appropriate.

### Who do I talk to? ###

The repo owner/admin is Dr. Mihaly Varadi (contact: mvaradi at ebi.ac.uk)