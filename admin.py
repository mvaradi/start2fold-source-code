from django.contrib import admin

from .models import MolecularSystem

admin.site.register(MolecularSystem)

# Register your models here.
