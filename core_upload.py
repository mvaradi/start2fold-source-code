"""
Data uploading methods
"""
import xml.etree.ElementTree as ET

from django.http import HttpResponse

from .models import *

def upload(request):

    # Uploading independent data
    #upload_secondary_structure_types()
    #upload_amino_acid_codes()
    #upload_exp_types()
    #upload_exp_methods()
    #upload_prot_levels()
    # upload_interface_element()
    upload_interface_content()

    # Uploading input XML file dependent data
    # tree = ET.parse("/Users/mvaradi/Django/mvaradi/start2fold/core_data/v1.0.0.xml")
    # root = tree.getroot()
    # data_crawler(root)
    return HttpResponse("Data uploaded")

def upload_interface_element():
	InterfaceContent.objects.create(content_name = "guide",
		content_header = "DOCUMENTATION").save()
	InterfaceContent.objects.create(content_name = "welcome",
		content_header = "WELCOME TO START2FOLD").save()

def upload_interface_content():
	welcome_texts = [u"Proteins are dynamic molecules that display a wide range of behaviours and fulfil many functions in the cell; understanding how they fold into complex three-dimensional structures and how these structures remain stable is essential for the interpretation of their overall behaviour. This database reports on the folding and stability of proteins based on hydrogen deuterium exchange (HDX) and oxidative labelling experimental data extracted from literature.",
	u"The protons of the backbone amide of amino acid residues exchange with protons of the solvent (water) at neutral or acidic pH.  If these amide protons are in residues that are part of the folding nuclei of proteins, they become protected from solvent at a very early stage during the folding process and stop exchanging. These 'protection rates' of amide protons can be detected by a range of methods that follow their solvent exchange from the completely unfolded state throughout the entire folding course: for example pulsed labelling, quenched flow and competition-based HDX measurements (coupled with either NMR or MS) as well as oxidative labelling experiments provide invaluable information on the folding mechanisms of proteins.",
	u"On the other hand, the native exchange experiments investigate proteins in their folded or partially folded states and report on the stability of existing hydrogen bonds to the amide protons based on their resistance to solvent exchange. By varying the environmental conditions, like pH or denaturant concentrations, such measurements can also provide quantitative information on the protection levels of the individual amide protons in function of changes in the conformation of the protein. Comparing these values can distinguish more stable from less stable regions of the protein fold.",
	u"Here we present a comprehensive collection of carefully curated and classified residue- or segment-level exchange data that report on the folding and/or stability proteins that have been previously investigated by solvent exchange-based measurements. Besides the classification of residues based on their exchange protection levels, all entries contain the detailed descriptions of the underlying experimental procedures, sample components, measurement conditions, cross references to other databases, references to the original publications and allow for the instant visualisation of the relevant residue groups on the 3D structures of the corresponding proteins."]
	guide_texts = [u"Start2Fold is a relational database implemented in MySQL. Data in Start2Fold is structured hierarchically in the following manner: On the top level (level 1) is the entry, which might be associated with one or multiple molecular systems (i.e. a protein of multiple chains, or even complexes of multiple proteins). Each entry has a distinct identifier, which consist of a tag (STF) followed by a 4 letter code (e.g. STF0001). An entry might have several associated protein chains (level 2). This level (or class) stores information on the UniProt and Protein Data Bank (PDB) IDs of the protein chain, and serves to provide direct cross-links to these online repositories. Each protein chain might have several corresponding experimental sets (level 3). These are either folding or stability experiments and are associated with a protection level (early, intermediate and late for folding and strong, medium and weak for stability measurements). Additional information recorded on the experimental sets include the resolution (residue-level or segment-level), the number of probes, the protection threshold, the experimental conditions (pH, temperature), the actual protein sequence used in the experiment (with any modifications/mutations) the PubMed ID of the original publication and a textual description of the experimental procedure. Finally, each experimental set has an array of amino acid residues that correspond to it (level 4). These residues provide the actual information on which positions/segments of the protein chain have which type of folding/stability information (i.e. which residues are early folding, etc.).",
	u"The user interface of Start2Fold is divided into 6 main sections. The 'home' section briefly introduces the database and the types of data contained within. It also provides the users with a contact form to send inquiries and feedback to the developers, and an integrated Tweet widget where the latest updates and news are posted. Next, the 'help' section contains the detailed documentation of the database, with an in-depth user guide describing all the functionalities of Start2Fold. Three sections provide browsing options by different criteria, such as browsing by proteins, residues sets or entries. Each option provides a list of entries and the most relevant information depending on the browsing option. When browsing by entries, the entry ID, molecular system name and whether the entry was reviewed or not are displayed. In case of browsing by proteins, the name of the molecular system, the recommended name of the protein, the entry ID, the UniProt and PDB IDs, the length of the protein chain and the secondary structure type of the chain constitute the browsing list. Lastly, when browsing by residue sets, the protection level, experiment type and method, entry ID, molecular system name and PubMed reference are displayed in the list. The user is forwarded to the accession screen by clicking on the entry ID links on either browsing list or the search results list. This page provides all the relevant information associated with the entry, along with an integrated JSmol applet (see next section). The 'home', 'help' and browsing section can be accessed from all the pages using the menu on the top section of the screen. Additionally, the database can be searched using the 'search' field located on the top left corner of the menu section. Searching Start2Fold can be performed by typing in protein names, UniProt/PDB IDs, experiment types, experiment methods and protection levels in the search field, and pressing 'search'.",
	u"The actual information stored within Start2Fold is displayed on the accession screens. The entry ID and the title of the entry are on the top of the page. Below is the 'download in xml' link, which provides the complete entry in XML format for downloading. This XML follows the structure of the XML template found on the welcome page. Alternatively, this XML can be directly accessed by adding '.xml' to the entry URL (e.g. start2fold.ibsquare/STF0004.xml).A JSmol applet is integrated below the download link. This applet can be used to visualize the different residue sets or segments by clicking on one of the buttons. The 'reset view' button can be clicked to reset the JSmol applet.The protein information and experimental set sections are right below the visualization applet. The protein information tab provides the name of the protein, the species of origin, the number of residues in the protein chain, and cross-links to UniProt and PDB. The experimental sets can be closed and opened by clicking on the 'show' and 'hide' buttons. These sections display the experimental type and method, the experimental conditions (pH, temperature, number of probes), a brief description of the experiment and the actual sequence that was used for the measurement. This sequence can be downloaded in FASTA format by clicking on the 'click to download' link under the sequence. Alternatively, the sequences can be directly accessed by adding '.fasta' to the URL (e.g. start2fold.ibsquare/STF0008.fasta). Finally, the residues are listed by their indices and their one-letter amino acid codes. The residue lists can be downloaded either by clicking the 'click to download' link below the residues, or by directly accessing them via adding '.residues' to the URL (e.g. start2fold.ibsquare/STF0008.residues)."]
	ref_elements = ["welcome", "guide"]
	for tag in ref_elements:
		if tag == "welcome":
			container = welcome_texts
		elif tag == "guide":
			container = guide_texts
		for text in container:
			ContentParagraphs.objects.create(content_name_ref_id = tag,
				text_paragraph = text).save()

def upload_secondary_structure_types():
    for ss_type in ["All-a", "All-b", "a+b", "b+a"]:
        SecondaryStructureType.objects.create(sec_str_type = ss_type).save()
        
def upload_amino_acid_codes():
    for aa in ["A", "C", "D", "E", "F", "G", "H", "I", "K", "L",
               "M", "N", "P", "Q", "R", "S", "T", "V", "W", "Y", "X"]:
        AminoAcid.objects.create(aa_code = aa).save()

def upload_exp_types():
    for exp_types in ["folding", "stability"]:
        ExperimentType.objects.create(exp_type = exp_types).save()

def upload_exp_methods():
    for method in ["Quenched-flow HDX NMR",
                     "Pulse labeling HDX MS",
                     "Pulse labeling HDX NMR",
                     "Burst phase labeling HDX NMR",
                     "Pulse labeling HDX NMR and MS",
                     "HDX-folding competition by NMR",
                     "pH-dependent HDX NMR",
                     "Dead time pulse labeling HDX NMR",
                     "Denatured state HDX NMR",
                     "Denaturant-dependent disappearance of amide cross peaks by NMR",
                     "Oxidative labeling MS",
                     "Native exchange NMR",
                     "Native exchange MS",
                     "Native exchange in partially folded state by NMR",
                     "Native exchange in partially folded state by MS"]:
        ExperimentMethod.objects.create(exp_method = method).save()

def upload_prot_levels():
    for level in ["EARLY", "INTERMEDIATE", "LATE", "STRONG", "MEDIUM", "WEAK"]:
        ProtectionLevel.objects.create(prot_lvl = level).save()

def upload_molecular_system(name):
    MolecularSystem.objects.create(mol_sys_name = name).save()

def upload_entry(primary_key, name_ref, curated):
    molsys = MolecularSystem.objects.get(pk = name_ref)
    molsys.entry_set.create(entry_id = primary_key,
                            manually_curated = curated)

def upload_chain(uniprot_id, ref_id, ss_class,
                 num_of_cis, num_of_res, pdb_id,
                 seq_start, seq_end, unip_seq,
                 pdb_sequence, ss_map):
    if num_of_cis == "?":
        num_of_cis = None
    ref_entry = Entry.objects.get(pk = ref_id)
    ref_ss = SecondaryStructureType(pk = ss_class)
    ref_entry.proteinchain_set.create(uniprot_code = uniprot_id,
                                      entry_id_ref = ref_entry,
                                      sec_str_type_ref = ref_ss,
                                      num_of_cis_bonds = num_of_cis,
                                      num_of_residues = num_of_res,
                                      pdb_code = pdb_id,
                                      pdb_seq = pdb_sequence,
                                      uniprot_seq = unip_seq,
                                      uniprot_seq_start = seq_start,
                                      uniprot_seq_end = seq_end,
                                      sec_str_mapping = ss_map).save()

def upload_exp_set(uniprot_id, experiment_type, experiment_method, expid,
                   resolution, PHmin, PHmax, PHnote, experiment_note, actual_seq,
                   temperature, numprobe, protthr, protlvl, pubmedid, cond_note,
                   residue_string, offset):
    if resolution == "True":
        resolution = 1
    elif resolution == "False":
        resolution = 0
    offset_resi_list = []
    splitted_string = residue_string.split(",")
    for resi in splitted_string:
        offset_resi_list.append(str(int(resi.strip()) +  int(offset) - 1))
    offset_resi_string = ", ".join(offset_resi_list)
    ref_uniprot = ProteinChain.objects.get(pk = uniprot_id)
    ref_exp_method = ExperimentMethod.objects.get(pk = experiment_method)
    ref_exp_type = ExperimentType.objects.get(pk = experiment_type)
    ref_prot_level = ProtectionLevel.objects.get(pk = protlvl)
    ExperimentSet.objects.create(pubmed_id = pubmedid,
                                 exp_id = expid,
                                 prot_lvl_ref = ref_prot_level,
                                 exp_method_ref = ref_exp_method,
                                 exp_type_ref = ref_exp_type,
                                 uniprot_code_ref = ref_uniprot,
                                 is_high_resolution = resolution,
                                 num_of_probes = numprobe,
                                 prot_threshold = protthr,
                                 exp_note = experiment_note,
                                 pH_min = PHmin,
                                 pH_max = PHmax,
                                 pH_note = PHnote,
                                 temp = temperature,
                                 actual_sequence = actual_seq,
                                 condition_note = cond_note,
                                 pdb_offset_list = offset_resi_string,
                                 residues = residue_string).save()

def upload_residues(seq_index, aa, expid):
    ref_aa = AminoAcid.objects.get(pk = aa)
    ref_exp = ExperimentSet.objects.get(pk = expid)
    Residue.objects.create(exp_id_ref = ref_exp,
                           aa_code_ref = ref_aa,
                           residue_code = seq_index).save()

def aux_data_parser():
    """
    Open and parse the auxillary data file into
    a dictionary
    """
    data = {}
    try:
        with open("/Users/mvaradi/Django/mvaradi/start2fold/core_data/aux.data") as aux_data:
            line_index = 0
            for line in aux_data:
                line_index += 1
                if line_index == 1:
                    pdb_id, chain_id, uniprot_id, exp_set_id = line.strip().split(",")
                    if uniprot_id not in data.keys():
                        data[uniprot_id] = {}
                    if exp_set_id not in data[uniprot_id].keys():
                        data[uniprot_id][exp_set_id] = {"sequence" : "",
                                                        "residues" : ""}
                elif line_index == 2:
                    data[uniprot_id][exp_set_id]["sequence"] = line.strip()
                elif line_index == 3:
                    data[uniprot_id][exp_set_id]["residues"] = line.strip()
                elif line_index == 4:
                    line_index = 0
            return data
    except IOError as ioerr:
        print "File error: %s" % ioerr   

def data_crawler(root):
    aux_data = aux_data_parser()
    entry_counter = 0
    # VERY IMPORTANT: SET THE EXP COUNTER TO THE APPROPRIATE NUMBER!
    exp_set_counter = 0
    for protein in root:
        mol_sys_name = protein.attrib["fullName"]
        
        """UPLOADING THE MOL SYS DATA"""
        # upload_molecular_system(mol_sys_name)

        entry_counter += 1
        entry_id = "STF%s%s" % ((4 - len(str(entry_counter))) * '0',
                                str(entry_counter))
        #IMPORTANT! MODIFY THE ID IF ADDITIONAL DATA IS BEING UPLOADED!
        curated = protein.attrib["checkedManually"]
        
        """UPLOADING THE ENTRY DATA"""
        #upload_entry(entry_id, mol_sys_name, curated)
        
        for chain in protein:
            if chain.tag == "chain":
                num_of_cis = chain.attrib["numCisBonds"]
                num_of_res = chain.attrib["numRes"]
                ss_class = chain.attrib["ssType"]
                
                str_level = chain.find("structure")
                pdb_id = str_level.attrib["pdbCode"]
                pdb_sequence = str_level.find("sequence").text
                ss_map = str_level.find("secStruc").text

                seq_level = chain.find("uniprot")
                seq_start, seq_end = seq_level.attrib["seqRange"].split("-")
                uniprot_id = seq_level.attrib["id"]
                unip_seq = seq_level.find("sequence").text
                
                """ UPLOADING THE CHAIN DATA """
                # upload_chain(uniprot_id, entry_id, ss_class,
                #             num_of_cis, num_of_res, pdb_id,
                #             seq_start, seq_end, unip_seq,
                #             pdb_sequence, ss_map)
                
                for sets in chain:
                    if sets.tag == "set":
                        exp_set_counter += 1
                        cond_note = None
                        had_sequence_tag = False
                        had_conditions_tag = False
                        had_residue = False
                        resolution = sets.attrib["highResolution"]
                        experiment_type = sets.attrib["type"]
                        experiment_method = sets.attrib["method"]
                        set_id_in_exp = sets.attrib["id"]
                        actual_seq = aux_data[uniprot_id][set_id_in_exp]["sequence"]
                        for sublevel in sets:
                            if sublevel.tag == "note":
                                if had_sequence_tag:
                                    if had_conditions_tag:
                                        if had_residue:
                                            cond_note = sublevel.text
                                        else:
                                            experiment_note = sublevel.text
                            elif sublevel.tag == "sequence":
                                had_sequence_tag = True
                            elif sublevel.tag == "conditions":
                                had_conditions_tag = True
                                PHmin = sublevel.attrib["pHmin"]
                                PHmax = sublevel.attrib["pHmax"]
                                if "note" in sublevel.attrib.keys():
                                    PHnote = sublevel.attrib["note"]
                                else:
                                    PHnote = None
                                if "temperature" in sublevel.attrib.keys():
                                    temperature = sublevel.attrib["temperature"]
                                else:
                                    temperature = None
                            elif sublevel.tag == "numberProbes":
                                numprobe = sublevel.text
                            elif sublevel.tag == "protectionThreshold":
                                protthr = sublevel.attrib["threshold"]
                            elif sublevel.tag == "protectionLevel":
                                protlvl = sublevel.text
                            elif sublevel.tag == "reference":
                                pubmedid = sublevel.attrib["pubMedId"]
                                pubmedid = pubmedid.split(',')[0]
                        residues = aux_data[uniprot_id][set_id_in_exp]["residues"].split(",")
                        residue_list = []
                        for resi in residues:
                            resindx, aa = resi.split(".")
                            residue_list.append(resindx)
                            """ UPLOADING THE RESIDUES """
                            upload_residues(resindx, aa, exp_set_counter)
                        residue_string = ", ".join(residue_list)        

                        """ UPLOADING THE EXP SETS """
                        # upload_exp_set(uniprot_id,
                        #                experiment_type,
                        #                experiment_method,
                        #                exp_set_counter,
                        #                resolution,
                        #                PHmin,
                        #                PHmax,
                        #                PHnote,
                        #                experiment_note,
                        #                actual_seq,
                        #                temperature,
                        #                numprobe,
                        #                protthr,
                        #                protlvl,
                        #                pubmedid,
                        #                cond_note,
                        #                residue_string, seq_start)
