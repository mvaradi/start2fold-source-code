# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('entry_id', models.CharField(max_length=7, serialize=False, verbose_name=b'Accession ID', primary_key=True)),
                ('manually_curated', models.BooleanField(default=False, verbose_name=b'Is manually curated')),
            ],
        ),
        migrations.CreateModel(
            name='MolecularSystem',
            fields=[
                ('mol_sys_name', models.CharField(max_length=255, serialize=False, verbose_name=b"Molecular system's name", primary_key=True)),
            ],
        ),
        migrations.AddField(
            model_name='entry',
            name='mol_sys_name_ref',
            field=models.ForeignKey(verbose_name=b'Related molecular system', to='start2fold.MolecularSystem'),
        ),
    ]
