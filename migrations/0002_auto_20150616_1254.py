# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProteinChain',
            fields=[
                ('uniprot_code', models.CharField(max_length=10, serialize=False, verbose_name=b'UniProt ID', primary_key=True)),
                ('num_of_cis_bonds', models.IntegerField(null=True, verbose_name=b'Number of cys-bonds')),
                ('num_of_residues', models.IntegerField(verbose_name=b'Number of residues')),
                ('pdb_code', models.CharField(max_length=10, null=True, verbose_name=b'PDB code')),
                ('pdb_seq', models.TextField(null=True, verbose_name=b'PDB sequence')),
                ('uniprot_seq', models.TextField(verbose_name=b'UniProt sequence')),
                ('uniprot_seq_start', models.IntegerField(verbose_name=b'Index of the first residue')),
                ('uniprot_seq_end', models.IntegerField(verbose_name=b'Index of the last residue')),
                ('sec_str_mapping', models.TextField(null=True, verbose_name=b'Secondary structure mapping')),
                ('seq_mutations', models.TextField(null=True, verbose_name=b'Mutation')),
                ('entry_id_ref', models.ForeignKey(verbose_name=b'Related entry', to='start2fold.Entry')),
            ],
        ),
        migrations.CreateModel(
            name='SecondaryStructureType',
            fields=[
                ('sec_str_type', models.CharField(max_length=50, serialize=False, verbose_name=b'Secondary structure type', primary_key=True, choices=[(b'None', b'None'), (b'All-a', b'Alpha'), (b'All-b', b'Beta'), (b'a+b', b'Mostly alpha and some beta'), (b'b+a', b'Mostly beta and some')])),
            ],
        ),
        migrations.AddField(
            model_name='proteinchain',
            name='sec_str_type_ref',
            field=models.ForeignKey(verbose_name=b'Related secondary structure type', to='start2fold.SecondaryStructureType'),
        ),
    ]
