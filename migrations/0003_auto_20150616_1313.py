# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0002_auto_20150616_1254'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExperimentSet',
            fields=[
                ('exp_id', models.AutoField(serialize=False, verbose_name=b'Experiment ID', primary_key=True)),
                ('is_high_resolution', models.BooleanField(default=False, verbose_name=b'Is high resolution')),
                ('seq_is_pdb', models.BooleanField(default=False, verbose_name=b'UniProt matches PDB')),
                ('num_of_probes', models.IntegerField(null=True, verbose_name=b'Number of probes')),
                ('prot_threshold', models.TextField(null=True, verbose_name=b'Protection notes')),
                ('exp_note', models.TextField(null=True, verbose_name=b'Experiment notes')),
                ('pH_min', models.FloatField(verbose_name=b'Minimum pH')),
                ('pH_max', models.FloatField(verbose_name=b'Maximum pH')),
                ('pH_note', models.TextField(null=True, verbose_name=b'pH notes')),
                ('temp', models.FloatField(null=True, verbose_name=b'Temperature')),
                ('condition_note', models.TextField(null=True, verbose_name=b'Condition notes')),
            ],
        ),
        migrations.CreateModel(
            name='ExperimentType',
            fields=[
                ('exp_type', models.CharField(max_length=255, serialize=False, verbose_name=b'Experiment type', primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProtectionLevel',
            fields=[
                ('prot_lvl', models.CharField(max_length=100, serialize=False, verbose_name=b'Protection level', primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='PubMedID',
            fields=[
                ('pubmed_id', models.CharField(max_length=100, serialize=False, verbose_name=b'PubMed ID', primary_key=True)),
            ],
        ),
        migrations.AddField(
            model_name='experimentset',
            name='exp_type_ref',
            field=models.ForeignKey(verbose_name=b'Related experiment type', to='start2fold.ExperimentType'),
        ),
        migrations.AddField(
            model_name='experimentset',
            name='prot_lvl_ref',
            field=models.ForeignKey(verbose_name=b'Related protection level', to='start2fold.ProtectionLevel'),
        ),
        migrations.AddField(
            model_name='experimentset',
            name='pubmed_id_ref',
            field=models.ForeignKey(verbose_name=b'Related PubMed ID', to='start2fold.PubMedID'),
        ),
        migrations.AddField(
            model_name='experimentset',
            name='uniprot_code_ref',
            field=models.ForeignKey(verbose_name=b'Related protein chain', to='start2fold.ProteinChain'),
        ),
    ]
