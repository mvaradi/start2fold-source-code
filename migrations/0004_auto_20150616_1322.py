# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0003_auto_20150616_1313'),
    ]

    operations = [
        migrations.CreateModel(
            name='AminoAcid',
            fields=[
                ('aa_code', models.CharField(max_length=1, serialize=False, verbose_name=b'One letter amino acid code', primary_key=True)),
            ],
        ),
    ]
