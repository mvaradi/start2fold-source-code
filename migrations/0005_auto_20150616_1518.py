# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0004_auto_20150616_1322'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='experimentset',
            name='pubmed_id_ref',
        ),
        migrations.AddField(
            model_name='experimentset',
            name='pubmed_id',
            field=models.CharField(default=b'TBA', max_length=100, verbose_name=b'PubMed ID'),
        ),
        migrations.DeleteModel(
            name='PubMedID',
        ),
    ]
