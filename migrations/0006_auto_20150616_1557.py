# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0005_auto_20150616_1518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='secondarystructuretype',
            name='sec_str_type',
            field=models.CharField(max_length=50, serialize=False, verbose_name=b'Secondary structure type', primary_key=True, choices=[(b'All-a', b'Alpha'), (b'All-b', b'Beta'), (b'a+b', b'Mostly alpha and some beta'), (b'b+a', b'Mostly beta and some')]),
        ),
    ]
