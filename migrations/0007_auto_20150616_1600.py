# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0006_auto_20150616_1557'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='proteinchain',
            name='seq_mutations',
        ),
        migrations.AddField(
            model_name='proteinchain',
            name='chain_note',
            field=models.TextField(null=True, verbose_name=b'Notes on the chain'),
        ),
    ]
