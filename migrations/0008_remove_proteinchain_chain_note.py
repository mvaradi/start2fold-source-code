# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0007_auto_20150616_1600'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='proteinchain',
            name='chain_note',
        ),
    ]
