# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0008_remove_proteinchain_chain_note'),
    ]

    operations = [
        migrations.CreateModel(
            name='InterfaceContent',
            fields=[
                ('content_name', models.CharField(max_length=255, serialize=False, verbose_name=b'Name of the content', primary_key=True)),
                ('content_text', models.TextField(verbose_name=b'The actual text content')),
            ],
        ),
    ]
