# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0009_interfacecontent'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactMessages',
            fields=[
                ('message_id', models.AutoField(serialize=False, verbose_name=b'Message ID', primary_key=True)),
                ('user_name', models.CharField(max_length=255, verbose_name=b'User name')),
                ('user_email', models.CharField(max_length=255, verbose_name=b'User email')),
                ('message_date', models.DateTimeField(auto_now=True, verbose_name=b'Message date')),
                ('message_text', models.TextField(verbose_name=b'Message text')),
            ],
        ),
    ]
