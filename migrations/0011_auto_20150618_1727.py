# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0010_contactmessages'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='interfacecontent',
            name='content_text',
        ),
        migrations.AddField(
            model_name='interfacecontent',
            name='content_header',
            field=models.TextField(null=True, verbose_name=b'Main header'),
        ),
        migrations.AddField(
            model_name='interfacecontent',
            name='content_p1',
            field=models.TextField(null=True, verbose_name=b'First paragraph'),
        ),
        migrations.AddField(
            model_name='interfacecontent',
            name='content_p2',
            field=models.TextField(null=True, verbose_name=b'Second paragraph'),
        ),
    ]
