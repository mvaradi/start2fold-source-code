# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0011_auto_20150618_1727'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContentParagraphs',
            fields=[
                ('text_id', models.AutoField(serialize=False, verbose_name=b'Text ID', primary_key=True)),
                ('text_paragraph', models.TextField(verbose_name=b'Text of the paragraph')),
                ('content_name_ref', models.ForeignKey(verbose_name=b'Related interface element', to='start2fold.InterfaceContent')),
            ],
        ),
    ]
