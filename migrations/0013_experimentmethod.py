# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0012_contentparagraphs'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExperimentMethod',
            fields=[
                ('exp_method', models.CharField(max_length=255, serialize=False, verbose_name=b'Experiment method', primary_key=True)),
            ],
        ),
    ]
