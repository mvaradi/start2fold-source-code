# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0013_experimentmethod'),
    ]

    operations = [
        migrations.AddField(
            model_name='experimentset',
            name='exp_method_ref',
            field=models.ForeignKey(default='Native exchange MS', verbose_name=b'Related experiment method', to='start2fold.ExperimentMethod'),
            preserve_default=False,
        ),
    ]
