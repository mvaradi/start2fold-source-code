# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0014_experimentset_exp_method_ref'),
    ]

    operations = [
        migrations.AlterField(
            model_name='experimentset',
            name='num_of_probes',
            field=models.TextField(null=True, verbose_name=b'Number of probes'),
        ),
    ]
