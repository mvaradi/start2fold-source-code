# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0015_auto_20150623_0026'),
    ]

    operations = [
        migrations.CreateModel(
            name='Residue',
            fields=[
                ('residue_id', models.AutoField(serialize=False, verbose_name=b'Residue ID', primary_key=True)),
                ('residue_code', models.IntegerField(verbose_name=b'Residue index code')),
                ('aa_code_ref', models.ForeignKey(verbose_name=b'Related amino acid code', to='start2fold.AminoAcid')),
                ('exp_id_ref', models.ForeignKey(default=b'', verbose_name=b'Related experiment set', to='start2fold.ExperimentSet')),
            ],
        ),
    ]
