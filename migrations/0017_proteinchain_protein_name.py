# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0016_residue'),
    ]

    operations = [
        migrations.AddField(
            model_name='proteinchain',
            name='protein_name',
            field=models.CharField(max_length=255, null=True, verbose_name=b'Protein name in UniProt for listing'),
        ),
    ]
