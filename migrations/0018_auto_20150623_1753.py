# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0017_proteinchain_protein_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='interfacecontent',
            name='content_p1',
        ),
        migrations.RemoveField(
            model_name='interfacecontent',
            name='content_p2',
        ),
    ]
