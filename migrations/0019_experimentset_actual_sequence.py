# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0018_auto_20150623_1753'),
    ]

    operations = [
        migrations.AddField(
            model_name='experimentset',
            name='actual_sequence',
            field=models.TextField(default=b'', verbose_name=b'Protein sequence in the set'),
        ),
    ]
