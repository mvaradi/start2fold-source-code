# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0019_experimentset_actual_sequence'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='experimentset',
            name='seq_is_pdb',
        ),
    ]
