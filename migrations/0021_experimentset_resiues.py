# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0020_remove_experimentset_seq_is_pdb'),
    ]

    operations = [
        migrations.AddField(
            model_name='experimentset',
            name='resiues',
            field=models.TextField(default=b'', verbose_name=b'Residues as list string'),
        ),
    ]
