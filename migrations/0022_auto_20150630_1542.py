# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0021_experimentset_resiues'),
    ]

    operations = [
        migrations.RenameField(
            model_name='experimentset',
            old_name='resiues',
            new_name='residues',
        ),
    ]
