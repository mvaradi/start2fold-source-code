# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0022_auto_20150630_1542'),
    ]

    operations = [
        migrations.AddField(
            model_name='experimentset',
            name='is_pdb',
            field=models.BooleanField(default=True, verbose_name=b'Sequence is the same as PDB'),
        ),
    ]
