# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0023_experimentset_is_pdb'),
    ]

    operations = [
        migrations.AddField(
            model_name='proteinchain',
            name='pdb_offset',
            field=models.IntegerField(default=0, verbose_name=b'PDB sequence offset'),
        ),
    ]
