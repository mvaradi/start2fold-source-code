# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('start2fold', '0025_auto_20151002_1627'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='experimentset',
            name='pdb_offset',
        ),
        migrations.AddField(
            model_name='experimentset',
            name='pdb_offset_list',
            field=models.TextField(null=True, verbose_name=b'PDB sequence offset'),
        ),
    ]
