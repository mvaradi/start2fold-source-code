from django.db import models

class MolecularSystem(models.Model):
    """
    This is the top class, which stores only
    the name of the molecular system.
    """
    # Primary key
    mol_sys_name = models.CharField("Molecular system's name",
                                    primary_key = True,
                                    max_length = 255)

    def __str__(self):
        return self.mol_sys_name

class Entry(models.Model):
    """
    This class is wrapped around all the actual
    information, and records if the data was
    manually curated.
    """
    # Primary key
    entry_id = models.CharField("Accession ID",
                                primary_key = True,
                                max_length = 7)
    # Foreign key, referencing MolecularSystem
    mol_sys_name_ref = models.ForeignKey(MolecularSystem,
                                         verbose_name = "Related molecular system")
    manually_curated = models.BooleanField("Is manually curated",
                                           default = False)

    def __str__(self):
        return self.entry_id

class SecondaryStructureType(models.Model):
    """
    Secondary structure types as mapped onto
    the PDB file.
    """
    SS_TYPES = (
        ('All-a', 'Alpha'),
        ('All-b', 'Beta'),
        ('a+b', 'Mostly alpha and some beta'),
        ('b+a', 'Mostly beta and some'),
    )
    # Primary key
    sec_str_type = models.CharField("Secondary structure type",
                                    primary_key = True,
                                    choices = SS_TYPES,
                                    max_length = 50)

    def __str__(self):
        return self.sec_str_type

class ProteinChain(models.Model):
    """
    Protein chain information
    """
    # Primary key
    uniprot_code = models.CharField("UniProt ID",
                                    primary_key = True,
                                    max_length = 10)
    # Foreign key, referencing Entry
    entry_id_ref = models.ForeignKey(Entry,
                                     verbose_name = "Related entry")
    # Foreign key, referencing SecondaryStructureType
    sec_str_type_ref = models.ForeignKey(SecondaryStructureType,
                                         verbose_name = "Related secondary structure type")
    num_of_cis_bonds = models.IntegerField("Number of cys-bonds",
                                           null = True)
    num_of_residues = models.IntegerField("Number of residues")
    pdb_code = models.CharField("PDB code",
                                max_length = 10,
                                null = True)
    pdb_seq = models.TextField("PDB sequence",
                               null = True)
    uniprot_seq = models.TextField("UniProt sequence")
    uniprot_seq_start = models.IntegerField("Index of the first residue")
    uniprot_seq_end = models.IntegerField("Index of the last residue")
    sec_str_mapping = models.TextField("Secondary structure mapping",
                                       null = True)
    protein_name = models.CharField("Protein name in UniProt for listing",
                                    max_length = 255,
                                    null = True)

    def __str__(self):
        return self.uniprot_code

class ExperimentType(models.Model):
    """
    Experiment types
    """
    # Primary key
    exp_type = models.CharField("Experiment type",
                                primary_key = True,
                                max_length = 255)

    def __str__(self):
        return self.exp_type

class ExperimentMethod(models.Model):
    """
    Experiment methods
    """
    # Primary key
    exp_method = models.CharField("Experiment method",
                                  primary_key = True,
                                  max_length = 255)

    def __str__(self):
        return self.exp_method

class ProtectionLevel(models.Model):
    """
    Protection level
    """
    # Primary key
    prot_lvl = models.CharField("Protection level",
                                primary_key = True,
                                max_length = 100)

    def __str__(self):
        return self.prot_lvl
    
class ExperimentSet(models.Model):
    """
    Experiment set information
    """
    # Primary key
    exp_id = models.AutoField("Experiment ID",
                              primary_key = True)
    # Foreign key, referencing ProteinChain
    uniprot_code_ref = models.ForeignKey(ProteinChain,
                                         verbose_name = "Related protein chain")
    # Foreign key, referencing ExperimentType
    exp_type_ref = models.ForeignKey(ExperimentType,
                                     verbose_name = "Related experiment type")
    # Foreign key, referencing ExperimentMethod
    exp_method_ref = models.ForeignKey(ExperimentMethod,
                                     verbose_name = "Related experiment method")
    # Foreign key, referencing ProtectionLevel
    prot_lvl_ref = models.ForeignKey(ProtectionLevel,
                                     verbose_name = "Related protection level")
    pubmed_id = models.CharField("PubMed ID",
                                 max_length = 100,
                                 default = "TBA")
    is_high_resolution = models.BooleanField("Is high resolution",
                                             default = False)
    num_of_probes = models.TextField("Number of probes",
                                        null = True)
    prot_threshold = models.TextField("Protection notes",
                                      null = True)
    exp_note = models.TextField("Experiment notes",
                                null = True)
    pH_min = models.FloatField("Minimum pH")
    pH_max = models.FloatField("Maximum pH")
    pH_note = models.TextField("pH notes",
                               null = True)
    temp = models.FloatField("Temperature",
                             null = True)
    condition_note = models.TextField("Condition notes",
                                      null = True)
    actual_sequence = models.TextField("Protein sequence in the set",
                                       default = "")
    residues = models.TextField("Residues as list string",
                               default = "")
    is_pdb = models.BooleanField("Sequence is the same as PDB",
                                 default = True)
    pdb_offset_list = models.TextField("PDB sequence offset",
                                     null = True)

    def __str__(self):
        return self.exp_id

class AminoAcid(models.Model):
    """
    One letter amino acid code
    """
    aa_code = models.CharField("One letter amino acid code",
                               primary_key = True,
                               max_length = 1)

    def __str__(self):
        return self.aa_code

class Residue(models.Model):
    """
    Residue level information
    """
    # Primary key
    residue_id = models.AutoField("Residue ID",
                                  primary_key = True)
    # Foreign key, referencing ExperimentSet
    exp_id_ref = models.ForeignKey(ExperimentSet,
                                   verbose_name = "Related experiment set",
                                   default = "")
    # Foreign key, referencing AminoAcid
    aa_code_ref = models.ForeignKey(AminoAcid,
                                    verbose_name = "Related amino acid code")
    residue_code = models.IntegerField("Residue index code")

    def __str__(self):
        return self.residue_id

class InterfaceContent(models.Model):
    """
    Text to be displayed on the online user
    interface
    """
    # Primary key
    content_name = models.CharField("Name of the content",
                                    primary_key = True,
                                    max_length = 255)
    content_header = models.TextField("Main header",
                                      null = True)

class ContentParagraphs(models.Model):
    """
    Text associated with interface content elements
    """
    # Primary key
    text_id = models.AutoField("Text ID",
                               primary_key = True)
    # Foreign key, referencing InterfaceContent
    content_name_ref = models.ForeignKey(InterfaceContent,
                                         verbose_name = "Related interface element")
    text_paragraph = models.TextField("Text of the paragraph")

class ContactMessages(models.Model):
    """
    Contact messages from the users
    """
    # Primary key
    message_id = models.AutoField("Message ID",
                                  primary_key = True)
    user_name = models.CharField("User name",
                                 max_length = 255)
    user_email = models.CharField("User email",
                                  max_length = 255)
    message_date = models.DateTimeField("Message date",
                                        auto_now = True)
    message_text = models.TextField("Message text")






