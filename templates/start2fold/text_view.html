{% load staticfiles %}
{% load extra_filters %}
{% include "start2fold/header.html" %}

<main class="container">

  <div class="row">
    <div id="text_content" class="col-xs-12">
      {% if welcome %}
      <h1>Welcome to Start2Fold!</h1>
      <h4>There are {{ num_of_entries }} entries in Start2Fold as of {{ today }}.
      We welcome additional data submissions! Please fill in and send the template below to <a href="mailto:mvaradi@ebi.ac.uk">this address <span class="glyphicon glyphicon-envelope"></span></a>:<br>
        <a href="{% static '/static/start2fold/template.xml' %}">
        XML template&nbsp;<span class="glyphicon glyphicon-download"></span>
        </a>
      </h4>
        <div class="row">
          <div class="col-md-3 bold center"><h2>What is here?</h2></div>
          <div class="col-md-9"><p>Start2Fold is a comprehensive collection of carefully curated and classified residue- or segment-level data on the folding and/or stability proteins that have been previously investigated by solvent exchange-based measurements. Besides the classification of residues based on their exchange protection levels, all entries contain the detailed descriptions of the underlying experimental procedures, sample components, measurement conditions, cross references to other databases, references to the original publications and instant visualisation of the relevant residue groups on the 3D structures of the corresponding proteins.</p></div>
        </div>

        <div class="row">
          <div class="col-md-3 bold col-md-push-9 center"><h2>Why is it relevant?</h2></div>
          <div class="col-md-9 col-md-pull-3"><p>Proteins are dynamic molecules that display a wide range of behaviours and fulfil many functions in the cell; understanding how they fold into complex three-dimensional structures and how these structures remain stable is essential for the interpretation of their overall behaviour. This database reports on the folding and stability of proteins based on hydrogen deuterium exchange (HDX) and oxidative labelling experimental data extracted from literature.</p></div>
        </div>

        <div class="row">
          <div class="col-md-3 bold center"><h2>Folding</h2></div>
          <div class="col-md-9"><p>The protons of the backbone amide of amino acid residues exchange with protons of the solvent (water) at neutral or acidic pH.  If these amide protons are in residues that are part of the folding nuclei of proteins, they become protected from solvent at a very early stage during the folding process and stop exchanging. These 'protection rates' of amide protons can be detected by a range of methods that follow their solvent exchange from the completely unfolded state throughout the entire folding course: for example pulsed labelling, quenched flow and competition-based HDX measurements (coupled with either NMR or MS) as well as oxidative labelling experiments provide invaluable information on the folding mechanisms of proteins.</p></div>
        </div>

        <div class="row">
          <div class="col-md-3 bold col-md-push-9 center"><h2>Stability</h2></div>
          <div class="col-md-9 col-md-pull-3"><p>On the other hand, the native exchange experiments investigate proteins in their folded or partially folded states and report on the stability of existing hydrogen bonds to the amide protons based on their resistance to solvent exchange. By varying the environmental conditions, like pH or denaturant concentrations, such measurements can also provide quantitative information on the protection levels of the individual amide protons in function of changes in the conformation of the protein. Comparing these values can distinguish more stable from less stable regions of the protein fold.</p></div>
        </div>

        <div class="row">
        	<div class="col-xs-12">
        		<h6>Please cite the database by referring to R. Pancsa and M. Varadi, P. Tompa, W. Vranken: <a href="http://nar.oxfordjournals.org/content/early/2015/11/17/nar.gkv1185.full" target="_blank">Start2Fold: a database of hydrogen/deuterium exchange data on protein folding and stability <span class="glyphicon glyphicon-link"></span></a> Nucleic Acids Research, November 2015, Database issue</h6>
        	</div>
        </div>

      {% elif user_guide %}

      <h1>Documentation</h1>
      <div class="row">
        <div class="col-md-3 bold center"><h3>Implementation</h3></div>
        <div class="col-md-9"><p>Start2Fold is a <strong>relational database</strong> implemented in <strong>MySQL</strong> using <strong>Django</strong>. Data in Start2Fold is structured hierarchically in the following manner:</p> 
        <ol>
          <li> <strong>Level 1:</strong> <p>At the root level is the entry, which might be associated with one or multiple molecular systems (i.e. a protein of multiple chains, or even complexes of multiple proteins). Each entry has a distinct identifier, which consist of a tag (STF) followed by a 4 letter code (e.g. STF0001).</p></li>
          <li> <strong>Level 2:</strong> <p>An entry might have several associated protein chains. This level (or class) stores information on the UniProt and Protein Data Bank (PDB) IDs of the protein chain, and serves to provide direct cross-links to these online repositories.</p></li>
          <li> <strong>Level 3:</strong> <p>Each protein chain might have several corresponding experimental sets. These are either folding or stability experiments and are associated with a protection level:</p> 
            <ul>
              <li>early</li>
              <li>intermediate</li> 
              <li>and late for <strong>folding</strong></li> 
              <li>strong</li> 
              <li>medium</li>
              <li>and weak for <strong>stability</strong> measurements </li>
            </ul>
            <p>Additional information recorded on the experimental sets include the resolution (residue-level or segment-level), the number of probes, the protection threshold, the experimental conditions (pH, temperature), the actual protein sequence used in the experiment (with any modifications/mutations) the PubMed ID of the original publication and a textual description of the experimental procedure.</p></li>
          <li><strong>Level 4:</strong> <p>Finally, each experimental set has an array of amino acid residues that correspond to it. These residues provide the actual information on which positions/segments of the protein chain have which type of folding/stability information (i.e. which residues are early folding, etc.).</p></li></div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-3 bold center"><h3>User interface</h3></div>
        <div class="col-md-9"><p>The user interface of Start2Fold is divided into the following main sections:
          <ul>
            <li><strong>Home: </strong><p>The 'home' section briefly introduces the database and the types of data contained within.</p></li> 
            <li><strong>Help: </strong><p>Next, the 'help' section contains the detailed documentation of the database, with an in-depth user guide describing all the functionalities of Start2Fold.</p></li>
            <li><strong>Contact: </strong><p>You are invited to send your questions and feedback using the contact form.</p></li>
            <li><strong>Browse: </strong><p>Browsing is enabled by different criteria, such as browsing by proteins, residues sets or entries. Each option provides a list of entries and the most relevant information depending on the browsing option. When browsing by entries, the entry ID, molecular system name and whether the entry was reviewed or not are displayed. In case of browsing by proteins, the name of the molecular system, the recommended name of the protein, the entry ID, the UniProt and PDB IDs, the length of the protein chain and the secondary structure type of the chain constitute the browsing list. Lastly, when browsing by residue sets, the protection level, experiment type and method, entry ID, molecular system name and PubMed reference are displayed in the list.</p></li>
            <li><strong>Search: </strong><p>The database can be searched using the 'search' field located on the top left corner of the menu section. Searching Start2Fold can be performed by typing in protein names, UniProt/PDB IDs, experiment types, experiment methods and protection levels in the search field, and pressing 'search'.</p></li>
            <li><strong>Entry: </strong><p>Clicking on the entry ID links on either browsing list or the search results list forwards you to the entry page. This page provides all the relevant information associated with the entry, along with a link to an integrated JSmol applet (see next section). The 'home', 'help', 'contact' and browsing sections can be accessed from all the pages using the menu on the top section of the screen.</p></li>
            </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-3 bold center"><h3>Entry pages</h3></div>
        <div class="col-md-9"><p>The actual information stored within Start2Fold is displayed on the accession screens.</p>
        <ul>
          <li><strong>Entry header: </strong><p>The entry ID and the title of the entry are on the top of the page.</p></li>
          <li><strong>Download: </strong><p>Below is the 'download in xml' link, which provides the complete entry in XML format for downloading. This XML follows the structure of the XML template found on the welcome page. Alternatively, this XML can be directly accessed by adding '.xml' to the entry URL (e.g. start2fold.eu/STF0004.xml).</p></li>
          <li><strong>Protein: </strong> <p>The protein information tab provides the name of the protein, the species of origin, the number of residues in the protein chain, and cross-links to UniProt and PDB.</p></li>
          <li><strong>JSmol app: </strong> <p>The link to an integrated JSmol applet can be used to visualize the different residue sets or segments by clicking on one of the buttons. The 'reset view' button can be clicked to reset the JSmol applet.</p></li> 
          <li><strong>Exp.set: </strong><p>The experimental sets can be closed and opened by clicking on the 'show' and 'hide' buttons. These sections display the experimental type and method, the experimental conditions (pH, temperature, number of probes), a brief description of the experiment and the actual sequence that was used for the measurement. This sequence can be downloaded in FASTA format by clicking on the 'click to download' link under the sequence. Alternatively, the sequences can be directly accessed by adding '.fasta' to the URL (e.g. start2fold.eu/STF0008.fasta).</p></li> 
          <li><strong>Residues: </strong><p>Finally, the residues are listed by their indices and their one-letter amino acid codes. The residue lists can be downloaded either by clicking the 'click to download' link below the residues, or by directly accessing them via adding '.residues' to the URL (e.g. start2fold.eu/STF0008.residues).</p></li></div>
      </div>

      {% endif %}   
    </div> <!-- closing text_content -->
  </div> <!-- closing row -->
</main>

{% include "start2fold/footer.html" %}