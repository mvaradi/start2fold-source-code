from django.conf.urls import url

from . import core_upload, views

urlpatterns = [
	url(r'^$', views.index, name='index'),
    url(r'^(?P<browse_by>proteins|ids|residues|ids_molsys|proteins_id|proteins_uni|proteins_pdb|proteins_length|proteins_ss|residues_prot|residues_type|residues_method|residues_ref)$', views.browse, name="browse"),
    url(r'^upload$', core_upload.upload, name='upload'),
    url(r'^message$', views.send_message, name="send_message"),
    url(r'^guide$', views.guide, name='guide'),
    url(r'^search$', views.search, name='search'),
    url(r'^(?P<entry_id>STF[0-9]{4})$', views.entry, name="entry"),
    url(r'^(?P<entry_id>STF[0-9]{4}).jsmol$', views.jsmol, name="jsmol"),
    url(r'^(?P<entry_id>STF[0-9]{4}).fasta$', views.fasta, name="fasta"),
    url(r'^(?P<entry_id>STF[0-9]{4}).residues$', views.residues, name="residues"),
    url(r'^(?P<entry_id>STF[0-9]{4}).xml$', views.xml_entry, name="xml_entry"),
]
