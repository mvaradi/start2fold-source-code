import urllib, datetime
from xml.etree import ElementTree as ET

from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.views import generic
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.messages import get_messages
from django.db.models import Q
from .models import *

def index(request):
    """
    Rendering the index
    """
    num_of_entries = len(Entry.objects.all())
    today = datetime.date.today()
    messages = get_messages(request)
    return render(request, "start2fold/text_view.html", {"num_of_entries" : num_of_entries, 
                                                         "today" : today,
                                                         "welcome" : True,})

def guide(request):
    """
    Rendering the user guide page
    """
    messages = get_messages(request)
    return render(request, "start2fold/text_view.html", {"user_guide" : True})

def browse(request, browse_by):
    """
    Browsing by IDs, protein names or experiments
    """
    
    if browse_by == "proteins" or browse_by == "proteins_id" or browse_by == "proteins_uni" or browse_by == "proteins_pdb" or browse_by == "proteins_length" or browse_by == "proteins_ss":
      if browse_by == "proteins":
        query_set = ProteinChain.objects.select_related().all().order_by("protein_name")
      elif browse_by == "proteins_id":
        query_set = ProteinChain.objects.select_related().all().order_by("entry_id_ref")
      elif browse_by == "proteins_uni":
        query_set = ProteinChain.objects.select_related().all().order_by("uniprot_code")
      elif browse_by == "proteins_pdb":
        query_set = ProteinChain.objects.select_related().all().order_by("pdb_code")
      elif browse_by == "proteins_length":
        query_set = ProteinChain.objects.select_related().all().order_by("num_of_residues")
      elif browse_by == "proteins_ss":
        query_set = ProteinChain.objects.select_related().all().order_by("sec_str_type_ref")
      browsing_by = "browsing proteins"
      return render(request, "start2fold/table_view.html", {"message" : browsing_by.upper(),
                                                           "query_set" : query_set,
                                                           "browse_view" : True,
                                                           "by_protein" : True})
    elif browse_by == "ids" or browse_by == "ids_molsys":
      if browse_by == "ids":
        query_set = Entry.objects.select_related().all().order_by("entry_id")
      elif browse_by == "ids_molsys":
        query_set = Entry.objects.select_related().all().order_by("mol_sys_name_ref")
      browsing_by = "browsing entries"
      return render(request, "start2fold/table_view.html", {"message" : browsing_by.upper(),
                                                           "query_set" : query_set,
                                                           "browse_view" : True,
                                                           "by_ids" : True})
    elif browse_by == "residues" or browse_by == "residues_prot" or browse_by == "residues_type" or browse_by == "residues_method" or browse_by == "residues_ref":
        if browse_by == "residues":
          query_set = ExperimentSet.objects.select_related().all().order_by("exp_id")
        elif browse_by == "residues_prot":
          query_set = ExperimentSet.objects.select_related().all().order_by("prot_lvl_ref")
        elif browse_by == "residues_type":
          query_set = ExperimentSet.objects.select_related().all().order_by("exp_type_ref")
        elif browse_by == "residues_method":
          query_set = ExperimentSet.objects.select_related().all().order_by("exp_method_ref")
        elif browse_by == "residues_ref":
          query_set = ExperimentSet.objects.select_related().all().order_by("pubmed_id")
        browsing_by = "browsing residue sets"
        return render(request, "start2fold/table_view.html", {"message" : browsing_by.upper(),
                                                           "query_set" : query_set,
                                                           "browse_view" : True,
                                                           "by_resi" : True})

def search(request):
    """
    Search functionality for the database.
    Searching is only allowed for:
    * Protein names
    * UniProt IDs
    * PDB IDs
    * Experiment types
    * Experiment methods
    * Protection levels
    """
    search_term = request.POST["search"]
    searchable = ExperimentSet.objects.select_related().all()
    query_set = searchable.filter(Q(uniprot_code_ref__protein_name__icontains=search_term) |
                                  Q(exp_type_ref__exp_type__icontains=search_term) |
                                  Q(prot_lvl_ref__prot_lvl__icontains=search_term) |
                                  Q(exp_method_ref__exp_method__icontains=search_term) |
                                  Q(uniprot_code_ref__uniprot_code__icontains=search_term) |
                                  Q(uniprot_code_ref__pdb_code__icontains=search_term)).distinct()
    return render(request, "start2fold/table_view.html", {"query_set" : query_set,
                                                        "search_view" : True,
                                                        "term" : search_term,})

def entry(request, entry_id):
    entry = Entry.objects.get(pk=entry_id)
    protein_block = ProteinChain.objects.select_related().all()
    protein_set = protein_block.filter(entry_id_ref__entry_id__icontains=entry_id)
    exp_block = ExperimentSet.objects.select_related().all()
    exp_set = exp_block.filter(uniprot_code_ref__entry_id_ref__entry_id__contains=entry_id)
    resi_block = Residue.objects.select_related().all()
    resi_set = resi_block.filter(exp_id_ref__uniprot_code_ref__entry_id_ref__entry_id__contains=entry_id)

    # THIS SHOULD BE MODIFIED IF MULTIPLE PROTEINS ARE ADDED
    # TO THE SAME ENTRY
    for protein in protein_set:
        uniprot_id = protein.uniprot_code
        uniprot_pull = pull_from_uniprot(uniprot_id).strip().split("\t")
        species = uniprot_pull.pop(0)
        taxonomy = "-".join(uniprot_pull)

    references = {}
    for exp in exp_set:
        exp_id = exp.exp_id
        pubmed_id = exp.pubmed_id
        references[exp_id] = PubMedPull(pubmed_id).get_data()

    return render(request, "start2fold/entry_view.html", {"entry" : entry,
                                                       "protein_set" : protein_set,
                                                       "exp_set" : exp_set,
                                                       "resi_set" : resi_set,
                                                       "entry_view" : True,
                                                       "is_search" : True,
                                                       "id" : entry_id,
                                                       "species" : species,
                                                       "taxonomy" : taxonomy,
                                                       "references" : references})

def jsmol(request, entry_id):
    entry = Entry.objects.get(pk=entry_id)
    protein_block = ProteinChain.objects.select_related().all()
    protein_set = protein_block.filter(entry_id_ref__entry_id__icontains=entry_id)
    exp_block = ExperimentSet.objects.select_related().all()
    exp_set = exp_block.filter(uniprot_code_ref__entry_id_ref__entry_id__contains=entry_id)

    return render(request, "start2fold/jsmol_view.html", {"entry" : entry,
                                                       "protein_set" : protein_set,
                                                       "exp_set" : exp_set,
                                                       "jsmol_view" : True,
                                                       "is_search" : True,
                                                       "id" : entry_id})


def xml_entry(request, entry_id):
    entry = Entry.objects.get(pk=entry_id)
    protein_block = ProteinChain.objects.select_related().all()
    protein_set = protein_block.filter(entry_id_ref__entry_id__icontains=entry_id)
    exp_block = ExperimentSet.objects.select_related().all()
    exp_set = exp_block.filter(uniprot_code_ref__entry_id_ref__entry_id__contains=entry_id)
    resi_block = Residue.objects.select_related().all()
    resi_set = resi_block.filter(exp_id_ref__uniprot_code_ref__entry_id_ref__entry_id__contains=entry_id)

    # THIS SHOULD BE MODIFIED IF MULTIPLE PROTEINS ARE ADDED
    # TO THE SAME ENTRY
    for protein in protein_set:
        uniprot_id = protein.uniprot_code
        uniprot_pull = pull_from_uniprot(uniprot_id).strip().split("\t")
        species = uniprot_pull.pop(0)

    return render(request, "start2fold/entry.xml", {"entry" : entry,
                                                    "protein_set" : protein_set,
                                                    "exp_set" : exp_set,
                                                    "resi_set" : resi_set,
                                                    "entry_view" : True,
                                                    "is_search" : True,
                                                    "id" : entry_id,
                                                    "species" : species},
                  content_type="text/xml")

def fasta(request, entry_id):
    exp_block = ExperimentSet.objects.select_related().all()
    exp_set = exp_block.filter(uniprot_code_ref__entry_id_ref__entry_id__contains=entry_id)
    return render(request, "start2fold/fasta.html", {"exp_set" : exp_set})

def residues(request, entry_id):
    entry = Entry.objects.get(pk=entry_id)
    resi_block = Residue.objects.select_related().all()
    resi_set = resi_block.filter(exp_id_ref__uniprot_code_ref__entry_id_ref__entry_id__contains=entry_id)
    return render(request, "start2fold/residues.html", {"resi_set" : resi_set,
                                                       "entry_id" : entry_id})

def send_message(request):
    """
    Storing the user messages 
    """
    try:
        contact_name = request.POST["contact_name"]
        contact_email = request.POST["contact_email"]
        contact_message = request.POST["contact_message"]
        if contact_name and contact_email and contact_message:
            ContactMessages.objects.create(user_name = contact_name,
                                           user_email = contact_email,
                                           message_text = contact_message).save()
            messages.success(request, "Thank you for your feedback!")
            return HttpResponseRedirect(reverse("start2fold:index"))
        else:
            previous_message = contact_message
            messages.error(request, "Please fill in every field!")
            content = get_object_or_404(InterfaceContent, pk = "welcome")
            paragraphs = get_list_or_404(ContentParagraphs, content_name_ref = content)
            return render(request, "start2fold/generic.html", {"content" : content,
                                                                "paragraphs" : paragraphs,
                                                                "previous_message" : previous_message,
                                                                "show_side_blocks" : True,
                                                                "text_view" : True})                                                  
    except:
        error_message = "Failed to send the message!"
        content = get_object_or_404(InterfaceContent, pk = "welcome")
        paragraphs = get_list_or_404(ContentParagraphs, content_name_ref = content)
        return render(request, "start2fold/generic.html", {"content" : content,
                                                            "paragraphs" : paragraphs,
                                                            "show_side_blocks" : True,
                                                            "text_view" : True,
                                                            "error_message" : error_message})

def pull_from_uniprot(uniprot_id):
    """
    Requesting data from the UniProt database
    """
    uniprot_id = uniprot_id
    columns = ",".join(["organism",
                        "lineage(superkingdom)",
                        "lineage(kingdom)",
                        "lineage(phylum)",
                        "lineage(class)",
                        "lineage(order)",
                        "lineage(family)"])
    output_format = "tab"

    requestURL = "http://www.uniprot.org/uniprot/?"
    requestURL += "query=%s&columns=%s&format=%s" % (uniprot_id,
                                                     columns,
                                                     output_format)

    pull_from_uniprot = urllib.urlopen(requestURL)
    pull_from_uniprot.next()
    return pull_from_uniprot.next()

class PubMedPull(dict):
    """
    This class has methods to pull data directly
    from the PubMed database, and store it in
    dictionary format.
    """

    def __init__(self, pubmed_id):
        """
        Initializing the storage dictionary
        """
        dict.__init__({})
        self.id = pubmed_id
        self.update({"title"    : "",
                     "authors"  : "",
                     "journal"  : "",
                     "pub_year" : "",
                     "volume_info" : ""})
        self.data = None

    def get_data(self):
        """
        Main method
        """
        self.request_data()
        self.parse_xml()
        if len(self["authors"].split()) > 6:
            self["authors"] = " ".join(self["authors"].split()[0:6])
            self["authors"] += " et al"
        citation_format = "%s. %s %s. %s;%s" % (self["authors"],
                                              self["title"],
                                              self["journal"],
                                              self["pub_year"],
                                              self["volume_info"])
        return citation_format

    def request_data(self):
        """
        Construct the request and send it
        to UniProt. The output format is
        XML.
        """
        requestURL = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi"
        requestURL += "?db=pubmed&id=%s&version=2.0" % self.id
        self.data = urllib.urlopen(requestURL)

    def parse_xml(self):
        """
        Parse the XML tree and extract the
        required information to be stored
        in the dictionary
        """
        tree = ET.parse(self.data)
        root = tree.getroot()
        doc_level = root.find("DocumentSummarySet") \
                           .find("DocumentSummary")

        for sub_level in doc_level:
            if sub_level.tag == "PubDate":
                self["pub_year"] = sub_level.text.split()[0]
                #print self["pub_year"]
            elif sub_level.tag == "Source":
                self["journal"] = doc_level.find("Source").text
                #print self["journal"]
            elif sub_level.tag == "Authors":
                for author in sub_level:
                    if not self["authors"] == "":
                        self["authors"] += ", "
                    self["authors"] += "%s" % author.find("Name").text
                #print self["authors"]
            elif sub_level.tag == "Title":
                self["title"] = sub_level.text
                #print self["title"]
            elif sub_level.tag == "Volume":
                self["volume_info"] += "%s(" % sub_level.text
            elif sub_level.tag == "Issue":
                self["volume_info"] += "%s):" % sub_level.text
            elif sub_level.tag == "Pages":
                self["volume_info"] += sub_level.text
                #print self["volume_info"]
                break
